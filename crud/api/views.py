from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import DestroyAPIView
from rest_framework.generics import RetrieveUpdateAPIView

from crud.api.serializers import CampaniaSerializer
from crud.models import Campania

# api de get y post de Compañia
class CampaniaApiView(APIView):

    def get(self, request):
        campanias = CampaniaSerializer(Campania.objects.all(), many=True)
        return Response(data=campanias.data, status=status.HTTP_200_OK)

    def post(self, request):
        Campania.objects.create(name=request.POST['name'],
                                description=request.POST['description'],
                                start_date=request.POST['start_date'],
                                end_date=request.POST['end_date'],
                                image=request.POST['image'])
        return self.get(request)

# api delete Compañia
class CampaniaApiDelete(DestroyAPIView):
    serializer_class = CampaniaSerializer
    queryset = Campania.objects.all()


# api update de Compañia
class CampaniaApiUpdate(RetrieveUpdateAPIView):
    serializer_class = CampaniaSerializer
    queryset = Campania.objects.all()
