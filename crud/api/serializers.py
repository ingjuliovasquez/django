from rest_framework import serializers


class CampaniaSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()
    description = serializers.CharField
    start_date = serializers.DateField()
    end_date = serializers.DateField()
    image = serializers.CharField()
