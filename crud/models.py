from django.db import models


# Create your models here.

class Campania(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    start_date = models.DateField()
    end_date = models.DateField()
    image = models.CharField(max_length=100)
